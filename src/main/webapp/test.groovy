if (!session) {
  session = request.getSession(true)
}

if (!session.counter) {
  session.counter = 1
}

println """
<html>
    <head>
        <title>Groovy Servlet</title>
    </head>
    <body>
        <p>
Hello, ${request.remoteHost}: ${session.counter}! ${new Date()}
<p>"""
println("<p>ifconfig:<br>")
try {
println("ifconfig".execute().text.replaceAll( '\n', '<br>' ))
     } catch(Exception ex){
println("no ifconfig in system")
}

println("<p>")
println("<p>ip addr:<br>")
try {
println("ip addr".execute().text.replaceAll( '\n', '<br>' ))
     } catch(Exception ex){
println("no ip addr in system")
}

println("<p>")
println("<p>ipconfig:<br>")
try {
println("ipconfig".execute().text.replaceAll( '\n', '<br>' ))
     } catch(Exception ex){
println("no ipconfig in system")
}

println("<p>")
println("<p>hostname:<br>")
try {
println("hostname".execute().text)
     } catch(Exception ex){
println("no hostname command")
}

println """
        </p>
    </body>
</html>
"""
session.counter = session.counter + 1
